import React, { Component } from 'react';
import { getList, addItem, deleteItem, updateItem } from './ListFunctions';

class List extends React.Component{
    constructor(){
        super()
        this.state={
            id:'',
            title:'',
            editDisabled:false,
            items:[]
        }
     this.onSubmit=this.onSubmit.bind(this)
     this.onChange=this.onChange.bind(this)
    }

    componentDidMount(){
        this.getAll();
    }

    onChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    getAll = () => {
        getList().then( data => {
            this.setState({
                title: '',
                items:[...data]
            },
            () => {
                console.log(this.state.items)
            })
        })
    }

    onSubmit = e => {
        e.preventDefault()
        addItem(this.state.title).then(() => {
            this.getAll()
        })
        this.setState({
            title: ''
        })
    }

    onUpdate = e => {
        e.preventDefault()
        updateItem(this.state.title, this.state.id).then(() => {
            this.getAll()
        })
        this.setState({
            title: '',
            editDisabled: ''
        })
        this.getAll()
    }

    onEdit = (val , e) => {
        e.preventDefault()
        var data = [...this.state.items]
        console.log(data, "data")
        data.map((item) => {
            if(item.id === val) {
                this.setState({
                    id: item.id,
                    title: item.title,
                    editDisabled: true
                })
            }
        })  
    }

    onDelete = (val, e) => {
        e.preventDefault()
        deleteItem(val)
        console.log(val, "id deleted")
        this.getAll()
    }


    render(){
        return(
            <div>
                <div className="col-md-12">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-12">
                                <input className="form-control"
                                    id="title"
                                    name="title"
                                    type="text"
                                    placeholder="enter your schedule here !!"
                                    value={this.state.title || ''}
                                    onChange={this.onChange.bind(this)} />
                                </div>
                            </div>
                        </div>
                        {!this.state.editDisabled ? (
                            <button type="submit"
                            className="btn btn-success btn-block mb-5"
                            onClick={this.onSubmit.bind(this)}>
                                Submit
                            </button>
                        ) : (
                            ''
                        )}
                        {this.state.editDisabled ? (
                            <button type="submit"
                            className="btn btn-success btn-block mb-5"
                            onClick={this.onUpdate.bind(this)}>
                                Update
                            </button>
                        ) : (
                            ''
                        )}
                    </form>
                    <h1 className="text-center">Things you need to do</h1>
                    <table className="table table-striped">
                        <thead class="thead-light">
                            <tr>
                            <th className="text-left">#</th>
                            <th className="text-left">Id</th>
                            <th className="text-left">Title</th>
                            <th className="text-center">Edit</th>
                            <th className="text-center">Done</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.items.map((item, index) => (
                                <tr key={index}>
                                    <td className="text-left">{index}</td>
                                    <td className="text-left">{item.id}</td>
                                    <td className="text-left">{item.title}</td>
                                    <td className="text-center">
                                        <button
                                            href=""
                                            className="btn btn-info mr-1"
                                            disabled={this.state.editDisabled}
                                            onClick={this.onEdit.bind(
                                                this,
                                                item.id
                                            )}>
                                            Edit
                                        </button>
                                    </td>
                                    <td className="text-center">
                                        <button
                                            href=""
                                            className="btn btn-danger"
                                            disabled={this.state.editDisabled}
                                            onClick={this.onDelete.bind(
                                                this,
                                                item.id
                                            )}>
                                            Done
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>   
                    </table>
                </div>
            </div>
        )


    }
}
export default List;