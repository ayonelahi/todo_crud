import React, { Component } from 'react';
import List from './List';

class App extends Component {
  render() {
    return (
     <div className="container">
       <div className="row">
         <div className="col-md-8 offset-md-2">
          <h1 className="text-center">Todo List App </h1>
          <p className="text-center">Don't miss a damn thing !! BAKAA !!</p>
          <List />
         </div>
       </div>
     </div>
    );
  }
}

export default App;
